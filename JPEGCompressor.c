#include <stdio.h>
#include <dirent.h>

int main(void)
{
    DIR *mydir = opendir("src");

    struct dirent *entry = NULL;
    
    while((entry = readdir(mydir))) {
		if (strlen(entry->d_name) > 2) {
			char command[256];
			
			printf("Compressing %s...\n", entry->d_name);
			
			sprintf(command, "lib\\ImageMagick-6.8.6-2\\convert.exe -strip -interlace Plane -resample 72 -resize 1000x1000 -quality 85 src\\%s dest\\%s", entry->d_name, entry->d_name);
			
			printf(command);
			printf("\n\n");
			
			system(command);
		}
    }
	
    closedir(mydir);
    return 0;
}